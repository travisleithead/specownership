// Tests for the Schema Generator
let fs = require("./helper/filesystem");
let SchemaGenerator = require("./schema-generator-2");
let testCount = 0;

async function schemaTest( dataSourceOb, expectedString, errorOutputFile ) {
    let sg = new SchemaGenerator();
    await sg.processDataSource( dataSourceOb );
    let schemaResultString = sg._dumpFullSchema();
    if ( expectedString != schemaResultString ) {
        await fs.writeFile( errorOutputFile, schemaResultString );
        throw Error( `Schema test failed (test ${ testCount }). Expected output written to ${ errorOutputFile }` );
    }
    // else passes!
}

async function callTest() {
    testCount++;
    await schemaTest( 
        JSON.parse( await fs.readFile( `test-schema/test-datasource-${ testCount }.json`, { encoding: "utf-8" } ) ),
        await fs.readFile( `test-schema/test-datasource-schema-${ testCount }.json`, { encoding: "utf-8" } ),
        `test-schema/test-datasource-schema-${ testCount }-actual.json` 
    );
}

module.exports = exports = async function runTests() {
    // Test 1 - Very "regular" (no variations) data source
    await callTest();
    // Test 2 - Missing properties from objects (1 of 5 for 80% present). Tests optional property detection in types
    await callTest();
    // Test 3 - Data Type located at two principal locations in a single dataset.
    await callTest();
    
    return testCount;
};