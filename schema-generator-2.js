const spinAsync = require( "./helper/spin-generator" );
const Timer = require("./helper/clock");
const ROOT = "_ROOT_";
// Schema Generator: the Next Generation

// A data source may contain multiple record groups (see "test-datasource-4"). A record 
// group may be split across different parts of the datasource (see "test-datasource-3"),
// thus having multiple paths through the datasource to get to the complete record group.

// The input to this process is a datasource. The output is a schema and path(s) to the record
// container object(s).

// The path encodes all the information needed to successfully traverse the datasource to find
// the record container. Paths are arrays of the following object structure:
// { type: "typename", next: ... }

// * type      = is always a string (the string name of the type). Use this string as the key to
//               dereference into the schema to get the full type info (e.g., to know how to 
//               precisely detect the given type in the dataset)
// * next      = value depends on what the kind of 'type' is:
//   - null    = For basic types. Basic types are leaf nodes in the type tree, so there is no
//               'next' that makes sense.
//   - "prop"  = For object or array types. The string can be a property name or index. The string
//               is the key of the object, or the array index to follow to get to the next object
//               in the path. (The type of "prop" will match the 'type' of the next path segment.)
//   - []      = For lists (records or arrays). The array or record must be iterated in order to
//               locate the next item in the path (there is no specific property or index that can
//               reliably be used to follow the path in the data). The 'type' can provide more detail
//               on what the expected set of types contained by the list may be.

// Types
// There are 3 general categories of Types that are produced by the Schema generator: basic, object, 
// and list types. Type names are always strings.
// BASIC       = These types have no formal definition and are assumed to be "built-in" by any
//               schema processor. Basic types can be identified easily by a 'typeof' operator.
//  "null"     = The null type. In general, "null" types are only seen in the schema when only null
//               values are encountered for a particular member. When used in combination with other
//               types they are ommited and the member is annotaed as "nullable" -- meaning: can
//               contain null values.
//  "string"  
//  "number"
//  "boolean"  = Pretty self-explanitory :-)
//  "emptyobject" An object with no keys.
//  "emptyarray" An array with no keys.
// OBJECT      = Object types are strongly matched object signatures, where the property names are 
//               significant. Note: an object type includes arrays when the array is "acting like" an
//               object, in that the array's positional indexes are significant.
//  "object*xx"= An "optional" object type. This type is an object where all the properties of the 
//               object are optional--to detect this type of object, all the set of known members of
//               the type must be checked. If any one of them is present, then this type is a match 
//               for the object.
//  "object!xx"= A "required" object type. This type is an object where at least one property *will* 
//               be present on the object to enable fast detection of this type (the required property)
//  "arraytype[T]" Similar to the "required" object type, this is a "required" array type. This type
//               is an array where the first array element (T) allows ready identification of the type.
//               The array indexes are positionally-significant.
// LIST        = List types are collections where no identifiable patterns exist among the types 
//               belonging to the list. Objects or arrays that match a LIST type must be iterated to
//               find an expected value (no other way of detection).
//  "record<T>"= An object type whose keys are all unique and have the same value type (T). Note, only
//               one value type T is allowed. And the object must have at least two keys.
//  "array<T|T>"=An array type, whose positions will include the noted types (T) in any position of the
//               array.

// Notes on new system

// First pass - For objects, a type is based on its key names. Thus, an object type can be deduced 
//              without needing to know anything about the types of the associated keys, and can be
//              trivially identified immediately. Objects are grouped together whenever they have 
//              keys that align. These become object types. Arrays are saved as-is (as array instances)
//              and are also grouped by their first array element for later analysis to see if the
//              group can become an array type. Since an array type is determined based on its
//              contents (rather than its array indexes), an array type cannot be determined until 
//              the algorithm has visited all its members.

// Second pass- Consists of a series of "finalizations", including marking the locations of the 
//              identified "record container" the type with the most instance counts, which (in 
//              most cases) should be where the majority of the datasource's records are held. 
//              First the previously-collected object types are finalized--to see if they are 
//              records, required or optional objects. Then, the candidate arrays are checked to
//              see if they qualify as a potential array type. The resulting candidate set are then
//              finalized by ensuring their identifying zero-th element is unique among all the other
//              non-candidates (to avoid array ambitutity in identifying these types). For the 
//              failed candidates, these instances are grouped by their types (classes) and all
//              array instances are assigned a type. As part of that final process, "friendly" (unique)
//              names are calculated for all the types.

// Third pass - The connected type-graph is iterated again to build the path to the "record container"
//              identified previously.

// Note on distinguishability

// Grouping all relatable object properties together will result in groups that (as a unit) have all 
// distinguishable keys. Thus, any key in a group is a distinguishable key from any other group
// (important insight). Groups are checked for one or more keys that are *required* across all of the
// candidate types in the group. If one (or more) are found, then all the keys in the group will
// be united into a Type that has (at least) one required key (that's feature-detectable when 
// this type is later encountered). If zero keys are found to be present across all templates in the 
// group, the group can still be merged, but only as an optional type.


// Figuring out array type vs. class

// Both array types and array class assignment depend on have accurate representations of consolidated 
// array types. Both depend on the other being done first, which poses a deadlock. For example, a potential
// array type might contain an array at a given position. What is that array going to be? Anohter array type,
// or an array class? You can't figure out the type until you have the answer, and you can't get the answer 
// until you figure out the type. To solve this problem, a backtracking algorithm is used. ArrayTypes are 
// aggregated first based on their first element:
// 1. All arrays are given a first pass (breadth-first), and classified in one of three ways:
//    * Settled ArrayType   - the set of arrays are *definately* array types, they don't contain any nested 
//                            arrays that would cause a dependency and type-uncertainty. They types can be
//                            immediately assigned.
//    * Not an ArrayType    - the set of arrays are definately not going to ever be an array type candidate.
//                            Positionally, the types will not line up.
//    * Candidate ArrayType - The arrays *might* be an array type, but it depends on how nested sub-arrays
//                            are resolved.
// 2. Resolution of Candidate ArrayTypes. To resolve these, you have to take a guess at what a resolution
//    for the nested sub-arrays might be. If your guess proves to be testably good, then you're done! But if
//    your guess is wrong, then you have to throw away all your assumptions and start over. This process works
//    as follows:
//    1. Assume all Candidate ArrayTypes will be actual Settled ArrayTypes (and assign types accordingly).
//    2. For all the "Not an ArrayType" arrays, form them into ArrayClasses. "Settled" array classes (which 
//       have no sub-arrays) are assigned a class and replace their sub-array entries in un-settled array classes
//       until there are no more un-settled array classes.
//    3. Steps 1 and 2 together form a new "virtual" array type system. Using this virtual array type system,
//       The set of candidate arraytypes (and their groups of unsettled sub-arrays) are re-evaluated (one arraytype
//       at a time) using the "virtual" types.
//    4. If every candidate ArrayType can be transformed into a Settled ArrayType, then this virtual array type
//       system was correct, and the arraytypes and arrayclasses derived from it are locked in.
//    5. If any one arraytype fails to form a "settled" arraytype (including if all but the last candidate arraytype
//       pass candidacy), then the "virtual" array type system collapses. The algorithm throws all of the virtual
//       types away and rejects the specific candidate array type that failed in the virtual world (re-classifies 
//       all the arrays it contained as "Not an ArrayType".
//    6. If there are no more Candidate ArrayTypes, then the leftover "Not an ArrayType" arrays can be grouped
//       into final array classes, and the algorithm ends. If there are still other Candidate ArrayTypes left to
//       check, then a new "virtual" array type system is constructed given the re-classified arrays, and the
//       process repeats from step 1.
// This process maximizes the number of possible ArrayTypes while ensuring that none of them are created on an
// incorrect classification of sub-arrays.

module.exports = exports = class SchemaGenerator2 {
    constructor() {
        this._typeManager = new TypeManager();
    }
    async processDataSource( dataSource ) {      
        await this._typeManager.processData( dataSource );
    }
    _dumpFullSchema() {
        return alphabeticalJSONStringify( this._typeManager.getSchema(), "" );
    }
}

function alphabeticalJSONStringify( jsonOb, indent ) {   
    let type = getBasicType( jsonOb );
    if ( type == "emptyarray" ) {
        return "[]";
    }
    if ( type == "emptyobject" ) {
        return "{}";
    }
    if ( type == "null" ) {
        return "null";
    }
    if ( type == "boolean" || type == "number" ) {
        return jsonOb;
    }
    if ( type == "string" ) {
        return `"${jsonOb}"`;
    }
    if ( type == "array" ) { 
        if ( typeof jsonOb.toJSON == "function" ) {
            jsonOb = jsonOb.toJSON();
        }
        let joiner = [];
        for ( let arrayEntryJsonOb of jsonOb ) {
            joiner.push( indent + "    " + alphabeticalJSONStringify( arrayEntryJsonOb, indent + "    " ) );
        }
        return `[\n${ joiner.join( ",\n" ) }\n${ indent }]`;
    }
    if ( type == "object" ) {
        if ( typeof jsonOb.toJSON == "function" ) {
            jsonOb = jsonOb.toJSON();
        }
        let joiner = [];
        let keys = Object.keys( jsonOb );
        keys.sort();
        for ( let key of keys ) {
            joiner.push( indent + "    " + `"${ key }": ` + alphabeticalJSONStringify( jsonOb[ key ], indent + "    " ) );
        }
        return `{\n${ joiner.join( ",\n" ) }\n${ indent }}`;
    }
    throw new Error( "Type not handled in JSON stringification" );
}

function* pauseOpportunity( timer ) {
    if ( timer.isExpired() ) {
        yield;
        timer.reset();
    }
}

// At this point, types are only strings or type objects with a "getName" function!
function* globalGetName( type, globalTypeMap, recursionBreaker, timer ) {
    if ( typeof type == "string" ) {
        return type;
    }
    else {
        yield* pauseOpportunity( timer );
        return yield* type.getName( globalTypeMap, recursionBreaker, timer );
    }
}

class DominantType {
    constructor() {
        this.best = new InstanceTypeTracker();
    }
    update( instanceTypeTracker ) {
        if ( instanceTypeTracker.maxCount > this.best.maxCount ) {
            this.best = instanceTypeTracker;
            delete this.best.data;
        }
    }
    greaterThan( otherDominantType ) {
        return ( this.best.maxCount > otherDominantType.best.maxCount );
    }
}

class InstanceTypeTracker {
    constructor( sourceType ) {
        this.source = sourceType;   // The type (ArrayType, ArrayClass, or ObjectType performing the tracking)
        this.data = new Map();      // Map of type => count (for this instance)
        this.maxCount = 0;          // The count of a certain type (maxType) that has occured in this instance
        this.maxType = null;        // The subtype that accounts for the most frequently seen subtype in source.
    }
    // Incrementally fill out this Type Tracker..
    add( type ) {
        let count = 1;
        if ( this.data.has( type ) ) {
            count = this.data.get( type );
            count++;
        }
        this.data.set( type, count );
        if ( count > this.maxCount ) {
            this.maxCount = count;
            this.maxType = type;
        }
    }
}

class TypeManager {
    constructor() {
        // Holds the primitive-type converted data source (after "buildTypeCandidateTree"), and the Type tree root (after pass 2).
        this.root = null;  
        // ( object => Type )
        // Direct references to all the objects encountered in the data source and their associated type (set in "linkObjects")
        this.objects = new Map();
        // ( Type => object ) inverse map; subset of this.objects
        // The candidate set of Records. Set by "linkObjects" and updated by "linkKnownRecords".
        this.potentialRecords = null; 
        // ( Type => object ) inverse map; subset of this.potentialRecords
        // The set of candidate Records whose sub-type is array-based. These can't be reliably grouped until
        // array processing happens (which is later), so they are tentatively grouped at first.
        // Set by "linkKnownRecords"
        this.arrayBasedRecords = null;

        this.arrays = [];  // Direct references to all the arrays encountered in the data source
                           //  Replaced with array->ArrayType|ArrayClass (generated in 'linkArrays';.
        this.allTypesMap = {}; // Filled by globalGetName()
        this.recordContainerString = ""; // Filled by locateRecordContainer        
        this.path = [];
    }
    async processData( data ) {
        let timer = new Timer(200);
        this.root = await spinAsync( this.buildTypeCandidateTree( data, timer ) );
        // linkObjects will assign ObjectType types to all potential record types...
        // updates "this.objects" and "this.potentialRecords"
        await spinAsync( this.linkObjects( timer ) ); // this.objects updated...
        // 1st pass at linking records...will be 100% successful as long as no records are based on array instances...
        await spinAsync( this.linkKnownRecords( timer ) );
        let objectMapCopy = null;
        do {
            objectMapCopy = new Map( this.objects );
        }
        while ( await spinAsync( this.conditionalLinkRecordsArrays( objectMapCopy, timer ) ) );
        // When existing, this.objects has the final type mapping.

        await spinAsync( this.finalizeObjectTypes( timer ) );
        // All types are interlinked at this point...
        // If root is an instance, hook it up to the Type
        if ( Array.isArray( this.root ) ) {
            this.root = this.arrays.get( this.root );
        }
        else if ( typeof this.root == "object" ) {
            this.root = this.objects.get( this.root );
        }
        let recursionBreaker = new Map();
        recursionBreaker.set("uniqueId", 1);
        this.allTypesMap[ ROOT ] = await spinAsync( globalGetName( this.root, this.allTypesMap, recursionBreaker, timer ) );
        this.recordContainerString = await spinAsync( this.locateRecordContainer( timer ) );
        this.calculatePathToContainer();
    }
    // This function traverses the raw data and does two things:
    // 1. Replaces all non-object, non-array instances with their types (e.g., strips
    //    the dataset of its specific values, replaceing them with built-in types).
    // 2. Gathers reference information on both objects and arrays in the dataset for
    //    later assembly/consolidation into Types.
    // Types will be either a string:
    // * "null"
    // * "string"
    // * "boolean"
    // * "number"
    // * "emptyobject"
    // * "emptyarray"
    // or an object reference (typeof "object"), in which case the object is either:
    // * object instance (not assigned to a type just yet)
    // * array instance (not assigned to a type just yet)
    *buildTypeCandidateTree( data, timer ) {
        let primaryType = getBasicType( data );
        if ( isLeafType( primaryType ) ) {
            return primaryType; // a string
        }
        // Note: the above catches empty arrays and empty objects as basic types.
        // It's not a leaf-type, so it must be an "object" or "array"
        if ( primaryType == "object" ) {
            this.trackObject( data );
            let keys = Object.keys( data );
            for ( let key of keys ) {
                data[ key ] = yield* this.buildTypeCandidateTree( data[ key ], timer );
                yield* pauseOpportunity( timer ); // Is it time for a break?
            }
            return data;
        }
        else if ( primaryType == "array" ) {
            this.trackArray( data );
            for ( let i = 0, len = data.length; i < len; i++ ) {
                data[ i ] = yield* this.buildTypeCandidateTree( data[ i ], timer );
                yield* pauseOpportunity( timer );
            }
            return data;
        }
        else {
            throw new Error( "Pass1: encountered some new unexpected non-basic type (which is not array or object)!" );
        }
    }
    getSchema() {
        return this.allTypesMap;
    }
    getPath() {
        return this.path;
    }
    trackObject( obj ) {
        this.objects.set( obj, true ); // The value-part is temporary.
    }
    trackArray( array ) {
        this.arrays.push( array );    
    }
    *linkObjects( timer ) {
        // Uses the object's keys to identify existing object type(s)
        // (can have more than one match). If there are no matches, then a new
        // ObjectType is created. If there is one match, then the keys are merged (and
        // it still might be possible to have required keys). If there are two or more
        // matches, then the union of keys are mapped and all the keys MUST be optional
        // (since joining two previously separate ObjectTypes will always be a disjoint 
        // set of properties).
        // Also sets an inverse map of potential records (ObjectType -> object)
        let objectTypeMapByKey = {};
        for ( let object of this.objects ) {
            let keys = Object.keys( object );
            let matchingObjectTypes = new Set();
            for ( let key of keys ) {
                if ( objectTypeMapByKey[ key ] ) {
                    matchingObjectTypes.add( objectTypeMapByKey[ key ] );
                }
            }
            if ( matchingObjectTypes.size == 0 ) { // No matches!
                let type = new ObjectType( keys );
                this.potentialRecords.set( type, object );
                // All of the type's keys will point to this type instance in the map
                for ( let key of keys ) {
                    objectTypeMapByKey[ key ] = type;
                }
            }
            else if ( matchingObjectTypes.size == 1 ) {
                let existingType = matchingObjectTypes.values().next().value;
                this.potentialRecords.delete( existingType ); // Removed because a record must be a singleton, multiple instances mean its a type match instead.
                // divide the keys into found and not-found buckets
                let found = new Set( keys );
                let notFound = new Set();
                for ( let key of keys ) {
                    if ( !objectTypeMapByKey[ key ] ) {
                        objectTypeMapByKey[ key ] = existingType; // This is a new indexable key for the future merged type
                        found.delete( key );
                        notFound.add( key );
                    }
                }
                existingType.mergeInstanceKeys( found, notFound );
            }
            else { // 2 or more matches found!
                let type = new ObjectType( keys );
                // NOTE: This *will* overwrite existing key->type mappings with the new type.
                for ( let key of keys ) {
                    objectTypeMapByKey[ key ] = type;
                }
                let found = new Set(); // Empty on purpose.
                for ( let existingType of matchingObjectTypes ) {
                    this.potentialRecords.delete( existingType ); // These are merging into one type, so not a record
                    // Pass empty 'found' set (marks all of existingType's keys as optional)
                    // Pass only the keys not in existingType's keys to ensure accurate optional key
                    // accounting...
                    let notFound = new Set( existingType.keys );
                    type.mergeInstanceKeys( found, notFound );
                    for ( let existingTypeKey of notFound ) {
                        // For convenience, this may re-assign a key to the same type. No harm done.
                        objectTypeMapByKey[ existingTypeKey ] = type;
                    }
                }
            }
            yield* pauseOpportunity( timer );
        }
        // iteration to create a complete object -> ObjectType mapping...
        let newObjectMap = new Map();
        for ( let object of this.objects.keys() ) {
            let key = Object.keys( object )[ 0 ];
            let type = objectTypeMapByKey[ key ];
            if ( type ) { // Any key will do...
                newObjectMap.set( object, type );
            }
            else {
                throw new Error( "Unexpected: all keys from every object should be present in the objectTypeMapByKey dictionary!" );
            }
        }
        this.objects = newObjectMap;
        yield* pauseOpportunity( timer );
        // this.potentialRecords; is up-to-date!
    }
    // Takes a map of ( object -> Type ) and replaces entries of Type with values in the inverse map
    // ( Type -> object ). Records any Types that are replaced in the replacedMap ( Type -> object )
    // and returns it.
    mergeInverseMap( sourceMap, inverseMap, typeOverride ) {
        let replacedMap = new Map();
        for ( let [ type, object ] of inverseMap ) {
            if ( typeOverride ) {
                type = typeOverride;
            }    
            if ( sourceMap.has( object ) ) {
                replacedMap.set( sourceMap.get( object ), object );
            }
            sourceMap.set( object, type );
        }
        return replacedMap;
    }
    // Responsible for taking the existing this.objects and this.potentialRecords and linking what
    // can be linked, returning the remainer of the unlinked potentialRecords and subset of those that were
    // specifically unlinkable due to array sub-type dependencies
    *linkKnownRecords( timer ) {
        let { potentialRecords, arrayBasedRecords } = yield* this.linkRecords( this.objects, this.potentialRecords, timer );
        this.potentialRecords = potentialRecords; // Store that back in the class' member
        this.arrayBasedRecords = arrayBasedRecords;
        // Note: rejectedRecords isn't used [yet]
    }
    // Manages assembling any left-over record types (based on arrays) with temporarty records,
    // and then testing via building actual array types, finaly confirming that the combining 
    // assumptions were true.
    *conditionalLinkRecordsArrays( objectMap, timer ) {
        // The following only necessary if there are entries in the arrayBasedRecords...
        let restoreMap = null;
        if ( this.arrayBasedRecords.size > 0 ) {
            // Assume all array-based records are the same array (will verify after 'linkArrays' to be sure)
            restoreMap = this.mergeInverseMap( objectMap, this.arrayBasedRecords, RecordType.createTemporaryType() );
            // Finalize the [hypothetical] records (and ensure there are no remaining potential records)
            let { potentialRecords, arrayBasedRecords } = yield* this.linkRecords( objectMap, this.potentialRecords, timer );
            // Note: rejectedRecords isn't used [yet]
            if ( potentialRecords.size != 0 || arrayBasedRecords.size != 0 ) {
                throw new Error( "Unexpected, linking Records with no unknown array types should resolve all outstanding records" );
            }
            // Note 'this.potentialRecords' and 'this.arrayBasedRecords' are not modified here (important!)
        }
        // add the linked arrays to the provided objectMap...
        yield* this.linkArrays( objectMap, timer );
        // every object and array has a type (ObjectType, RecordType, ArrayType, or ArrayClass) at this point, but these
        // assignments might be based on fiction. Time to verify...
        if ( this.arrayBasedRecords.size > 0 ) {
            // Check assumptions
            // Undo the temporaryArrayType assignments to the objectMap made earlier... (prep for linkRecords logic)
            let tempRecordMap = this.mergeInverseMap( objectMap, restoreMap );
            // Since all the array instances are in the objectMap, linkRecords will have no conditional results--if there
            // are any potentialRecords that fail--those break the assumptions made earlier, and the process must
            // restart. Otherwise, linkRecords assigns actual RecordType types to the record objects (instead of the 
            // temporary types assigned earlier)
            let { potentialRecords, arrayBasedRecords, rejectedRecords } = yield* this.linkRecords( objectMap, this.potentialRecords, timer );
            if ( arrayBasedRecords.size > 0 ) {
                throw new Error( "Unexpected: after linking arrays, there should be no potential records based on generic arrays" );
            }
            if ( rejectedRecords.size > 0 ) {
                // Check to what extent types need to be fixed...
                // How many rejected Records were in the arrayBasedRecords map?
                let arrayBasedRejectedRecords = [];
                for ( let [ type, object ] of rejectedRecords ) {
                    if ( this.arrayBasedRecords.has( type ) ) {
                        arrayBasedRejectedRecords.push( type );
                    }
                }
                if ( arrayBasedRejectedRecords.length == 0 ) {
                    throw new Error( "Unexpected: Any rejected records should have a root cause stemming from a rejecte array-based potential record rejection." );
                }
                // If the size is 1, and there is any failure, it's OK, since the one record would not have been merged
                // with any other record instance--therefore nothing is invalidated (the type was wrong though and has now
                // been corrected.)
                if ( this.arrayBasedRecords.size > 1 ) {
                    // Sadly, in this case, an array-type was opportunistically merged, but that turned out to be not-possible!
                    // Need to remove the not-possible array-based records from the possible set and try again (which won't 
                    // Merge these particular object-records).
                    arrayBasedRejectedRecords.forEach( type => {
                        this.potentialRecords.delete( type ); // This is the set that matters for the next iteration.
                    } );
                    yield* this.linkKnownRecords( timer ); // Uses the original this.objects and [just] modified this.potentialRecords
                    // as the basis for the next iteration.
                    return true; // Flag to try again.
                }
                else { // this.arrayBasedRecords == 1
                    if ( tempRecordMap.size != 1 ) {
                        throw new Error( "Unexpected: there must be only one temporary record created to enter this code path" );
                    }
                    // The type-damage is repairable without another full cycle!
                    // Fix any references in the array-type's that have the temp-objects in them
                    yield* this.validateArrayTypes( objectMap, tempRecordMap, timer );
                }
            }
        }
        // 
        this.objects = objectMap;
        return false;
    }
    // Takes a typeMap (object->Type initially, may include (array->Type later), and a potential record Map 
    // (ObjectType->object), and determines if the
    // potential record Map really are records... if so, it de-dups them by type, and puts them into the objectMap 
    // (removing them from the potential record Map as a result). If not, it leaves them in the potential record
    // map, and returns it and a new map of potential records that depend on arrays (mutually-exclusive from the
    // potential records map returned). If there are no records dependent on arrays, then an empty array map is 
    // returned, and the potential record map will also be empty (all records being resolved).
    // Passing 'true' for 'assertNoRemainder' ensures that all postential records are linked; otherwise the
    // method asserts (throws exception). If the potential records is NOT a record, this does not cause an assert.
    *linkRecords( typeMap, potentialRecords, timer ) {
        // Check potential Records to see if they are actual records
        let potentialRecordsStartingCount = 0;
        let updatedPotentialRecords = new Map(); // ( ObjectType => object )
        let arrayBasedRecords = new Map(); // ( ObjectType => object )
        let rejectedRecords = new Map(); // ( ObjectType => object )
        let recordMap = new Map();
        // Pre-fill the recordMap with any existing Records ( subtype -> Record ) from the current typeMap
        for ( let type of typeMap.values() ) {
            if ( type instanceof RecordType && type.getSubtype() != "testing" ) {
                // Excludes "temporary" record type objects (which will be those based on consolidated 'array' types)
                recordMap.set( type.getSubtype(), type );
            }
        }
        yield* pauseOpportunity( timer );
        do {
            potentialRecordsStartingCount = potentialRecords.size;
            // Record types that depend on another [unresolved] record type will stay in the 
            // potential record bucket, potentially prevening the count from dropping to zero.
            for ( let [ type, object ] of potentialRecords ) {
                let result = RecordType.checkCandidate( object, typeMap, potentialRecords );
                if ( result == -2 ) { // Can't (yet) resolve this record type (will need another iteration).
                    // Since object-based record types do not have loops in the original JSON data, it is
                    // guaranteed that there will be some record that bottoms-out at a leaf node and this
                    // starts to resolve other dependencies. (Unless it's an array type at the bottom.)
                    // ("object" stays in the potentialRecords list for another loop around).
                    updatedPotentialRecords.set( type, object );
                }
                else if ( result == -1 ) {
                    // This record type is not resolvable until the array's are processed. For record-type
                    // consolidation, this only impacts array processing if there are >1 of these potential 
                    // records-base-on-array-subtypes. (Since they will be assumed to combine, but may not be
                    // able to be.)
                    arrayBasedRecords.set( type, object );
                }
                else if ( result == 1 ) {
                    // Unconditionally a record type! *Replace* the ObjectType with a RecordType in the typeMap
                    let recordType = new RecordType( object, typeMap );
                    let recordSubType = recordType.getSubtype();
                    // Group/merge when the subtype is the same!
                    if ( recordMap.has( recordSubType ) ) {
                        recordType = recordMap.get( recordSubType ).merge( recordType );
                    }
                    else {
                        recordMap.set( recordSubType, recordType ); // For tracking (to de-dup)
                    }
                    typeMap.set( object, recordType ); // To be sure the next 'checkCandidate' incorporates it.
                    // Removed (not re-added) from the potentialRecords
                    // No need to record these successes.
                }
                else if ( result == 0 ) {
                    // Not a record type (not re-added for evaluation again)
                    rejectedRecords.set( type, object );
                }
                else {
                    throw new Error( "Unexpected return value from RecordType:checkCandidate!" );
                }
            }
            potentialRecords = updatedPotentialRecords;
            updatedPotentialRecords = new Map();
            yield* pauseOpportunity( timer );
        }
        while ( potentialRecords.size > 0 && potentialRecordsStartingCount != potentialRecords.size );
        if ( potentialRecords.size > 0 && arrayBasedRecords.size == 0 ) {
            throw new Error( "Unexpected! After record consolidation, the only reason to have outstanding potentialRecords is due to at least one outstanding array sub-type dependency" );
        }
        // The potentialRecords returned should always include the array-based map as a subset, so re-add 
        // those to the map now (they are not added previously to optimize the algorithm's loop).
        for ( let [ type, object ] of arrayBasedRecords ) {
            potentialRecords.set( type, object );
        }
        return {
            potentialRecords: potentialRecords,
            arrayBasedRecords: arrayBasedRecords,
            rejectedRecords: rejectedRecords
        };
    }
    *linkArrays( objectMap, timer ) {
        // There are no array types initially, just a collection of array instances. Types will be either 
        // ArrayType or ArrayClass.
        // Returns the map that would be created using this process.
        let arrayMapByEntryZero = new Map();
        // Step 1: grouping by 0th entry.
        for ( let originalArray of this.arrays ) {
            // Create a typed copy of the original array (which needs to stay 'original' in case this algorithm
            // is run again iteratively...)
            let array = [];
            for ( let i = 0, len = originalArray.length; i < len; i++ ) {
                let element = originalArray[ i ];
                if ( typeof element == "object" && !Array.isArray( element ) ) {
                    element = objectMap.get( element ); // Get's the Type instead of the instance.
                }
                array.push( element );
            }
            // For finding Array Type, track the 0th object--either an array instance, simple type, or ObjectType
            let firstEntryTypeKey = array[ 0 ];
            if ( Array.isArray( firstEntryTypeKey ) ) {
                firstEntryTypeKey = "array"; // Treat all these generically for now...
            }
            if ( arrayMapByEntryZero.has( firstEntryTypeKey ) ) {
                let groupList = arrayMapByEntryZero.get( firstEntryTypeKey );
                groupList.push( array );
                arrayMapByEntryZero.set ( firstEntryTypeKey, groupList );
            }
            else {
                arrayMapByEntryZero.set( firstEntryTypeKey, [ array ] );
            }
        }
        yield* pauseOpportunity( timer );
        // Step 2:Bucketize groups of arrayLists into Final ArrayType, Candidate ArrayTypes, and Not ArrayTypes
        let finalArrayMap = new Map(); // Final Array-to-ArrayType mapping!
        let candidateGroup = []; // Array lists that are candidate groups
        let notArrayType = []; // Individual array instances that don't qualify.
        for ( let arrayList of arrayMapByEntryZero.values() ) {
            let result = ArrayType.checkCandidate( arrayList );
            if ( result == 0 ) { // Definately not an ArrayType...
                arrayList.forEach( ( array ) => { 
                    notArrayType.push( array ); 
                } );
            }
            else if ( result > 0 ) { // Definately an ArrayType...
                let arrayType = new ArrayType( arrayList );
                // Note: no need to call "finalize" since there's gauranteed, no sub-arrays to resolve.
                arrayList.forEach( ( array ) => {
                    finalArrayMap.set( array, arrayType );
                } );
            }
            else { // < 0 (or might be an ArrayType)
                candidateGroup.push( arrayList );
            }
        }
        yield* pauseOpportunity( timer );
        // Step 3: Resolve candidate groups (must get to zero)
        while ( candidateGroup.length > 0 ) {
            // Treat the current set of candidate groups as authentic Array types....
            let tempArrayMap = new Map( finalArrayMap ); // Start with the final Types...
            let reverseTypeMap = new Map();
            candidateGroup.forEach( ( candidateList, i ) => {
                let arrayType = new ArrayType( candidateList );
                reverseTypeMap.set( arrayType, i ); // Tracks the index into the candidateGroup array from which the list originated.
                candidateList.forEach( ( array ) => {
                    tempArrayMap.set( array, arrayType );
                } );
            } );
            yield* pauseOpportunity( timer );
            // Treat everything else as array classes
            // Merge the new ArrayClasses Map into the tempArrayType Map...
            let newArrayClassesMap = ArrayClass.classify( notArrayType, tempArrayMap );
            let arrayClases = new Set();
            for ( let [ array, arrayClass ] of newArrayClassesMap ) {
                tempArrayMap.set( array, arrayClass );
                arrayClases.add( arrayClass );
            }
            yield* pauseOpportunity( timer );
            // Every array instance has a Type now...
            // Test the candidate array types to see if they all still work as array types...
            for ( let arrayType of reverseTypeMap.keys() ) {
                arrayType.finalize( tempArrayMap );
                // Ensure the arrayType's original candidate arrays would still pass, given the latest tempArrayMap
                let result = ArrayType.checkCandidate( candidateGroup[ reverseTypeMap.get( arrayType ) ], tempArrayMap );
                if ( result > 0 ) {
                    // Also, check that the ArrayType's requiredType is not present in any other existing ArrayClass
                    // (that would lead to potential ambiguity in ArrayType detection in the data source)
                    for ( let arrayclass of arrayClases ) {
                        if ( arrayclass.includes( arrayType.getRequiredType() ) ) {
                            result = 0; // An array class includes the proposed required type--keeping the type would
                            break;      // lead to ambiguity, so it must be discarded!
                        }
                    }
                }
                if ( result < 0 ) {
                    throw new Error( "Unexpected! All array instance should have a Type or Class at this point!" );
                }
                else if ( result == 0 ) {
                    // If test fails, then pull it out of the candidateGroup
                    let groupIndex = reverseTypeMap.get( arrayType );
                    let candidateList = candidateGroup[ groupIndex ];
                    // Transfer the failed candidate List to the netArrayType group.
                    candidateList.forEach( ( candidate ) => { 
                        notArrayType.push( candidate ); 
                    } );
                    // Remove the failed ArrayType candidate from the group...
                    candidateGroup.splice( groupIndex, 1 );
                    break;
                }
                // No problems with the ArrayType... keep checking...
                yield* pauseOpportunity( timer );
            }
            // IF this works, then clear the notArrayType list....
            if ( reverseTypeMap.size == candidateGroup.length ) {
                finalArrayMap = tempArrayMap;
                candidateGroup = [];
                notArrayType = [];
            }
        }
        if ( notArrayType.length > 0 ) { // If the previous loop was never entered...
            for ( let [ array, arrayClass ] of ArrayClass.classify( notArrayType, finalArrayMap ) ) {
                finalArrayMap.set( array, arrayClass );
            }
        }
        yield* pauseOpportunity( timer );
        // Integrate the finalArrayMap into the provided objectMap
        for ( let [ array, type ] of finalArrayMap ) {
            objectMap.set( array, type );
        }
        // objectMap updated. (does not need to be returned) No other permanent state changed.
    }
    // Checks that all array types in the map have the correct type mapping.
    // Called if there is a repairable change to the array type mapping (which 
    // consists of an array type that contains a reference to an object type that
    // itself references a TemporaryRecord object)
    *validateArrayTypes( objectMap, tempRecordMap, timer ) {
        for ( let [ object, type ] of objectMap ) {
            if ( type instanceof ArrayType ) {
                type.fixup( objectMap, tempRecordMap );
                pauseOpportunity( timer );
            }
            else if ( type instanceof ArrayClass ) {
                type.fixup( objectMap, tempRecordMap );
                pauseOpportunity( timer );
            }
        }
    }
    // Object types are certain, but don't contain inner member-data yet. Use the newly finalized
    // type mappings for objects and arrays to create the proper object member info.
    *finalizeObjectTypes( timer ) {
        let objectTypes = new Set();
        for ( let [ object, type ] of this.objects ) {
            objectTypes.add( type );
            let subtypeMap = new Map();
            let keys = Object.keys( object );
            for ( let key of keys ) {
                let subtype = object[ key ];
                if ( typeof subtype == "object" ) { // both arrays and objects handled together
                    subtype = this.objects.get( subtype );
                }
                subtypeMap.set( key, subtype );
            }
            type.addInstance( subtypeMap );
        }
        yield* pauseOpportunity( timer );
        // Now, finalize each object type, determining optional or required object type
        for ( let objectType of objectTypes ) {
            objectType.finalize();
        }
    }
    *locateRecordContainer( timer ) {
        // Simple (max) strategy; 
        // TODO: try to employ outlier stats to potentially find multi-modal records!
        let mostDominantType = new DominantType();
        let mostDominantTypeRef = null;
        let allTypesKeys = Object.keys( this.allTypesMap );
        for ( let key of allTypesKeys ) {
            let type = this.allTypesMap[ key ];
            if ( typeof type == "string" ) { 
                // This is ROOT's value (always a string ref)...
                continue;
            }
            if ( type.dominantType.best.maxCount < 1 ) {
                throw new Error( "Unexpected! For every type, there should be at least one instance!" );
            }
            if ( type.dominantType.greaterThan( mostDominantType ) ) {
                mostDominantType = type.dominantType;
                mostDominantTypeRef = type;
                yield* pauseOpportunity( timer );
            }
        }
        if ( mostDominantTypeRef == null ) {
            // No type objects even existed (or they'd have at least a dominantType count of 1)
            return this.allTypesMap[ ROOT ]; // Since there wasn't any type objects in the schema, the container is the root.
        }
        else {
            mostDominantTypeRef.recordContainer = true;
            return mostDominantTypeRef.name;
        }
    }
    calculatePathToContainer() {
        if ( !findPath( this.root, this.path ) ) { // Sync, because it's largely traversal--should be very fast
            if ( this.allTypesMap.size != 1 ) { // The one is for datasets that don't contain objects or arrays--it'll just have a simple string value at ROOT
                throw new Error( "Unexpected: Some path must be located!" );
            }
        }
    }
}

// ObjectTypes are worked on in two steps:
// 1. Type identification (based on key analysis)
// 2. Sub-type finalization (addInstance/finalize) done after all objects and array types/classes are seen
class ObjectType {
    constructor( initialKeysList ) {
        this.keys = initialKeysList;
        this.valueTypes = {};
        for ( let key of initialKeysList ) {
            // All keys start out as required on first-instance creation
            this.valueTypes[ key ] = { 
                flags: { 
                    optionality: "required", // | "optional"
                    nullableInstanceCount: 0
                },
                types: new Map() // of <type object or string> => { instanceCount: <num 1:N> }
            };
        }
        this.optionalKeyCount = 0;
        // The following are for record-type identification.
        this.dominantType = new DominantType();
    }
    mergeInstanceKeys( foundKeys, notFoundKeys ) {
        // Check if any of my existing keys are not found in the found keys from this instance...
        for ( let myKey of this.keys ) {
            if ( !foundKeys.has( myKey ) && this.valueTypes[ myKey ].flags.optionality != "optional" ) {
                // Mark myKey as optional.
                this.valueTypes[ myKey ].flags.optionality = "optional";
                this.optionalKeyCount++;
            }
        }
        for ( let newKey of notFoundKeys ) {
            // Ensure I don't double-count--as the caller can lie and pass-in duplicate "not found" keys.
            if ( !this.valueTypes[ newKey ] ) {
                this.keys.push( newKey );
                this.valueTypes[ newKey ] = { 
                    flags: { 
                        optionality: "optional",
                        nullableInstanceCount: 0
                    }, 
                    types: new Map() 
                    // typesString: <string> -- added at getName() time.
                };
                this.optionalKeyCount++;
            }
        }
    }
    addInstance( subtypeMap ) {
        let tracker = new InstanceTypeTracker();
        for ( let [ key, type ] of subtypeMap ) {
            if ( !this.valueTypes[ key ] ) {
                throw new Error( "Unexpected: all keys to-be-encountered should be already in the valueTypes dictionary" );
            }
            let typesMap = this.valueTypes[ key ].types;
            if ( type == "null" ) {
                // Null is not a type, but a Type-signal (that the underlying type can be null).
                // Since one key can have multiple types, a null applies to any of the types (because it is unknown which type it corresponds to)
                // null is *only* a type if it's the only possible value that is ever seen for this key
                this.valueTypes[ key ].flags.nullableInstanceCount++;
            }
            else if ( typesMap.has( type ) ) {
                typesMap.get( type ).instanceCount++;
                tracker.add( type );
            }
            else {
                typesMap.set( type, { instanceCount: 1 } );
                tracker.add( type );
            }
        }
        this.dominantType.update( tracker );
    }
    finalize() {
        this.keys.sort();
        // Will it be required or optional?
        this.type = "object";
        if ( this.optionalKeyCount == this.keys.length ) {
            this.typeDetect = "optional";
        }
        else {
            this.typeDetect = "required";
            for ( let key of this.keys ) {
                if ( this.valueTypes[ key ].flags.optionality == "required") {
                    this.requiredKey = key;
                    break;
                }
            }
            if ( !this.requiredKey ) {
                throw new Error( "Unexpected: required key should have been located in the valueTypes dictionary" );
            }
        }
    }
    *getName( globalTypeMap, recursionBreaker, timer ) {
        if ( this.name ) {
            return this.name; // Already did the work for this Type!
        }
        // Assign the name
        if ( this.type == "record" ) {
            // Records are the only ObjectType that depend on a sub-type for their name... break out
            // the recursion breaker!
            if ( recursionBreaker.has( this ) ) {
                let counter = recursionBreaker.get( "uniqueId" );
                let name = `record<recursive${ counter }>`;
                recursionBreaker.set( "uniqueId", counter + 1 );
                return name;
            }
            recursionBreaker.set( this, true );
            // Note, records require getting the subtype name in order to compute the final name...
            this.subtypeName = yield* globalGetName( this.subtype, globalTypeMap, recursionBreaker, timer );
            this.name = `record<${ this.subtypeName }>`;
        }
        else if ( this.type == "object" ) {
            // Set this.name before traversing the rest of the object's keys (to avoid re-entrancy)
            if ( this.typeDetect == "optional") {
                this.name = "object*"; // Partial name... finished below:
                if ( this.keys.length < 20 ) {
                    this.name += this.keys.reduce( ( prev, curr ) => { 
                        return `${ prev }${ curr.substr( 0, 2 ) }`; 
                    }, "" );
                }
                else {
                    this.name += this.keys.reduce( ( prev, curr, i ) => {
                        if ( i < 50 ) {
                            return `${ prev }${ curr.substr( 0, 1 ) }`;
                        }
                        else {
                            return prev;
                        }
                    }, "" );
                }
            }
            else if ( this.typeDetect == "required" ) {
                this.name = `object!${ this.requiredKey }`;
            }
            else {
                throw new Error( "Unexpected: unrecognized object type-detect" );
            }
            for ( let key of this.keys ) {
                let typesStringNames = [];
                for ( let type of this.valueTypes[ key ].types.keys() ) {
                    let typeStringName = yield* globalGetName( type, globalTypeMap, recursionBreaker, timer );
                    this.valueTypes[ key ].types.get( type ).name = typeStringName;
                    typesStringNames.push( typeStringName );
                }
                this.valueTypes[ key ].typesString = ( typesStringNames.length == 0 ) ? "null" : typesStringNames.join( "|" );   
            }
        }
        else {
            throw new Error( "Unexpected: unrecognized object type (not 'record' or 'object')" );
        }
        globalTypeMap[ this.name ] = this;
        return this.name;
    }
    containsRecordContainer( path ) {
        // If this has been previously visited, then return quickly.
        if ( this.pathChecked ) {
            return false; // Not on path or the previous visit would have found the path first
        }
        this.pathChecked = true;
        if ( this.recordContainer ) {
            path.unshift( new PathSeg( this.name, "null" ) );
            return true;
        }
        if ( this.type == "record" ) {
            if ( findPath( this.subtype, path ) ) {
                path.unshift( new PathSeg( this.name, this.type ) );
                return true;
            }
            return false;
        }
        else if ( this.type == "object" ) {
            // Go through all the stored valuetypes...
            for ( let key of this.keys ) {
                for ( let orType of this.valueTypes[ key ].types.keys() ) {
                    if ( findPath( orType, path ) ) {
                        path.unshift( new PathSeg( this.name, this.type, key ) );
                        return true;
                    }
                }
            }
            return false;
        }
        else {
            throw new Error( "Unexpected type parameter (or missing) in path checking" );
        }
    }
    toJSON() {
        let representative = {
            type: this.type
        };
        if ( this.type != "record" ) {
            // Gather valuetypes
            representative.keys = {};
            for ( let key of this.keys ) {
                representative.keys[ key ] = {
                    optional: ( this.valueTypes[ key ].flags.optionality == "optional" ),
                    nullable: ( this.valueTypes[ key ].flags.nullableInstanceCount > 0 ),
                    types: this.valueTypes[ key ].typesString,
                    stats: {
                        nullableCount: this.valueTypes[ key ].flags.nullableInstanceCount,
                        typeCount: {}
                    }
                };
                for ( let countOb of this.valueTypes[ key ].types.values() ) {
                    representative.keys[ key ].stats.typeCount[ countOb.name ] = countOb.instanceCount;
                }
            }
            representative.typeDetect = this.typeDetect;
            if ( this.typeDetect == "required" ) {
                representative.requiredKey = this.requiredKey;
            }
        }
        else { // a "record"
            representative.subtype = this.subtypeName;
            representative.nullable = ( this.nullableInstanceCount > 0 );
            representative.stats = {
                nullableCount: this.nullableInstanceCount,
                instanceCount: this.instanceCount
            };
        }
        if ( this.recordContainer ) {
            representative.PATH_TARGET = true;
        }
        return representative;
    }
}

class RecordType {
    // The provided object is assumed to be pre-validated by 'checkCandidate'.
    // 'typeMap' will already have all the dependent type mappings, as validated by 'checkCandidate' returning a 1.
    constructor( object, typeMap ) {
        this.type = "record";
        this.nullableInstanceCount = 0;
        let keys = Object.keys( object );
        for ( let key of keys ) {
            let value = object[ key ];
            if ( value != "null" ) {
                if ( typeof value == "object" ) {
                    value = typeMap.get( value ); // initialy, only for objects (not arrays) 
                }
                this.subtype = value; // value [should] be the same no matter which entry is picked; so 
                // overwriting the existing entry shouldn't matter.
            }
            else {
                this.nullableInstanceCount++;
            }
        }
        if ( !this.subtype ) {
            // e.g., if all types were "null"
            this.subtype = "null";
        }        
        this.instanceCount = keys.length;
    }
    getSubtype() {
        return this.subtype;
    }
    merge( recordType ) {
        if ( this.subtype != recordType.subtype ) {
            throw new Error( "Unexpected: bad merge! subtypes must match in order to merge!" );
        }
        this.nullableInstanceCount += recordType.nullableInstanceCount;
        this.instanceCount += recordType.instanceCount;
        return this;
    }
    // 1) Used to see if an object's key values meets "Record" criteria. Answer can be either
    //    >0 (1) - yes, the provided object instance constitutes a RecordType
    //    =0 (0) - no, the provided object instance is definately not a RecordType
    //    <0 (-1) - the provided object instance *might* be a RecordType (but can't be proven
    //              because it's Record sub-type is an array that hasn't been given a proper type yet
    //    <0 (-2) - the instance is a type being evaluated as another potential Record type
    // Prior to calling this, other RecordType criteria are met: e.g., the instance in question is a
    // one-of-a-kind ObjectType (doesn't share other instances), also meaning each key has only one value.
    static checkCandidate( instance, knownTypeMap, potentialRecordsMap ) {
        // To qualify, all keys much have the same type. If that type is an array, we can't know for 
        // sure yet :-)
        let keys = Object.keys( instance );
        if ( keys.length < 2 ) {
            return 0; // Must have at least two keys to be a record!
        }
        let allValues = new Set();
        let provisionalArray = false; // True if "array" substitutions are made...
        let provisionalRecord = false;
        for ( let key of keys ) {
            let value = instance[ key ];
            if ( value != "null" ) { // Skip 'null' values...(they're not types)
                // The following applies to objects AND array instances...
                knownType = knownTypeMap.get( value );
                if ( knownType ) { // Always true for object instances, might be true for array instances
                    value = knownType;
                    if ( potentialRecordsMap.has( value ) ) {
                        provisionalRecord = true;
                    }
                }
                else if ( Array.isArray( value ) ) {
                    value = "array"; // temp value for grouping...
                    provisionalArray = true;
                }
                allValues.add( value );
            }
        }
        if ( allValues.size == 0 ) { // Can happen if all the values were 'null'
            allValues.add( "null" );
        }
        if ( allValues.size == 1 ) { // A 'yes' or 'maybe'
            if ( provisionalArray ) {
                return -1;
            }
            else if ( provisionalRecord ) {
                return -2;
            }
            else {
                return 1;
            }
        }
        return 0; // A definate 'no'
    }
    static createTemporaryType() {
        let recordType = new RecordType( {} );
        recordType.subtype = "testing";
        return recordType;
    }
    *getName( globalTypeMap, recursionBreaker, timer ) {
    }
    containsRecordContainer( path ) {
    }
    toJSON() {
    }
}

// Only instantiated when the Type is confirmed (or conditionally-confirmed)
class ArrayType {
    constructor( arrayList ) {
        this.type = "arraytype";
        this.valueTypes = [];
        this.unresolvedSubArrays = new Set(); // Empty (no unresolved elements) or indexes.
        this.dominantType = new DominantType();
        let minRequired = arrayList[ 0 ].length;
        for ( let array of arrayList ) {
            let tracker = new InstanceTypeTracker();
            minRequired = Math.min( array.length, minRequired );
            for ( let i = 0, len = array.length; i < len; i++ ) {
                if ( !this.valueTypes[ i ] ) {
                    this.valueTypes[ i ] = { 
                        flags: { 
                            optionality: "required",
                            nullableInstanceCount: 0
                        }, 
                        instanceCount: 0,
                        type: "null" // Will be overridden *unless* all the values for each array are "null" at this point.
                        // typeString: <string> - populated in getName()
                    };
                }
                let type = array[ i ];
                let valueType = this.valueTypes[ i ]; 
                if ( Array.isArray( type ) ) {
                    this.unresolvedSubArrays.add( i );
                    tracker.add( type );
                }
                valueType.instanceCount++;
                if ( type != "null" ) {
                    valueType.type = type;
                    tracker.add( type );
                }
                else { // null
                    valueType.flags.nullableInstanceCount++;
                }
            }
            this.dominantType.update( tracker );
        }
        // Mark valueTypes after minRequired as optional.
        for ( let i = minRequired, len = this.valueTypes.length; i < len; i++ ) {
            this.valueTypes[ i ].flags.optionality = "optional";
        }
        this.requiredType = this.valueTypes[ 0 ].type;
        // requiredTypeString added later in getNames()
    }
    // If there are any unresolvedSubArrays, this resolves them.
    finalize( subArrayMap ) {
        for ( let index of this.unresolvedSubArrays ) {
            let subArrayType = subArrayMap.get( this.valueTypes[ index ].type );
            this.valueTypes[ index ].type = subArrayType;
            if ( index == 0 ) {
                this.requiredType = subArrayType;
            }
        }
        if ( Array.isArray( this.dominantType.best.maxType ) ) {
            this.dominantType.best.maxType = subArrayMap.get( this.dominantType.best.maxType );
        }
    }
    fixup( objectMap, tempRecordMap ) {
        if ( tempRecordMap.has( this.requiredType ) ) {
            // Use the ( type, object ) map to index into the ( object, type ) correct mapping.
            this.requiredType = objectMap.get( tempRecordMap.get( this.requiredType ) );
        }
        for ( let valueType of this.valueTypes ) {
            if ( tempRecordMap.has( valueType.type ) ) {
                valueType.type = objectMap.get( tempRecordMap.get( valueType.type ) );
            }
        }
    }
    getRequiredType() {
        return this.requiredType;
    }
    // Plays two roles:
    // 1) Used to see if an arbirary list of arrays meets "Type" criteria. Answer can be either
    //    >0 (1) - yes, the provided arrayList constitues an ArrayType
    //    =0 (0) - no, the provided arrayList is definately not an ArrayType
    //    <0 (-1) - the provided arrayList *might* be an ArrayType (but can't be proven because it
    //              contains a sub-array that hasn't been given a proper type yet.)
    static checkCandidate( arrayList, subArrayTypeMap ) {
        // If a candidate type has only one instance, it cannot be promoted to a Type.
        if ( arrayList.length <= 1 ) {
            return 0;
        }
        // Otherwise, all candidates must have type-matching positions (or be undefined)
        // Get max length of all the candidates...
        let maxLen = 0;
        for ( let array of arrayList ) {
            maxLen = Math.max( maxLen, array.length );
        }
        let hasSubArray = false;
        for ( let i = 0; i < maxLen ; i++ ) {
            // Get the first non-undefined, non-null type in the template
            let undefinedCount = 0;
            let type = null;
            for ( let pos = 0, len = arrayList.length; pos < len ; pos++ ) {
                type = arrayList[ pos ][ i ];
                if ( Array.isArray( type ) ) {
                    if ( subArrayTypeMap && subArrayTypeMap.has( type ) ) {
                        type = subArrayTypeMap.get( type );
                    }
                    else {
                        type = "array";
                        hasSubArray = true;
                    }
                }
                if ( type == undefined ) {
                    undefinedCount++;
                }
                else if ( type != "null" ) { // "null" and undefined are given a pass...
                    // Finish the previous loop, looking for any entries that don't exactly match
                    for ( let finishPos = pos + 1; finishPos < len; finishPos++ ) {
                        let type2 = arrayList[ finishPos ][ i ];
                        if ( Array.isArray( type2 ) ) {
                            if ( subArrayTypeMap && subArrayTypeMap.has( type2 ) ) {
                                type2 = subArrayTypeMap.get( type2 );
                            }
                            else {
                                type2 = "array";
                                hasSubArray = true;
                            }
                        }
                        if ( type2 != "null" && type2 != undefined && type !== type2 ) {
                            return 0; // not an array type
                        }
                        // Otherwise, the typeResults do match! Continue checking...
                    }
                    break; // The 'pos' loop is now complete per above, cancel the rest of its iteration
                }
            }
            if ( undefinedCount == arrayList.length ) {
                // Can get here if the entries contained only 'undefined' values -- this shouldn't happen in JSON data
                throw new Error( "Unexpected; encountered Array template candidates at position ${i} that have all undefined" );
            }
        }
        if ( hasSubArray ) {
            return -1; // Might be an ArrayType...
        }
        return 1; // Yes, it's an ArrayType
    }
    *getName( map, recursionBreaker, timer ) {
        if ( this.name ) {
            return this.name;
        }
        // Since this type requires a sub-type to have a name (to compute it's name), and because
        // that can lead to infinite recursion if the sub-type is another dependent-type, set an
        // intermediate type name, to be used to break the recursion
        if ( recursionBreaker.has( this ) ) {
            let counter = recursionBreaker.get( "uniqueId" );
            let name = `arraytyperecursive${ counter }`;
            recursionBreaker.set( "uniqueId", counter + 1 );
            return name;
        }
        recursionBreaker.set( this, true );
        this.requiredTypeString = yield* globalGetName( this.requiredType, map, recursionBreaker, timer );
        this.name = `arraytype[${ this.requiredTypeString }]`;
        // Get the typeString values for each of the valueTypes... (after setting the name)
        for ( let valueType of this.valueTypes ) {
            valueType.typeString = yield* globalGetName( valueType.type, map, recursionBreaker, timer );
        }
        map[ this.name ] = this;
        return this.name;
    }
    containsRecordContainer( path ) {
        // If this has been previously visited, then return quickly.
        if ( this.pathChecked ) {
            return false; // Not on path or the previous visit would have found the path first
        }
        this.pathChecked = true;
        if ( this.recordContainer ) {
            path.unshift( new PathSeg( this.name, "null" ) );
            return true;
        }
        for ( let i = 0, len = this.valueTypes.length; i < len; i++ ) {
            if ( findPath( this.valueTypes[ i ].type, path ) ) {
                path.unshift( new PathSeg( this.name, this.type, String(i) ) );
                return true;
            }   
        }
        return false;
    }
    toJSON() {
        let representative = {
            type: this.type,
            requiredType: this.requiredTypeString,
            keys: {}
        };
        // Gather valuetypes
        for ( let i = 0, len = this.valueTypes.length; i < len; i++ ) {
            representative.keys[ String( i ) ] = {
                optional: ( this.valueTypes[ i ].flags.optionality == "optional" ),
                nullable: ( this.valueTypes[ i ].flags.nullableInstanceCount > 0 ),
                type: this.valueTypes[ i ].typeString,
                stats: {
                    nullableCount: this.valueTypes[ i ].flags.nullableInstanceCount,
                    instanceCount: this.valueTypes[ i ].instanceCount
                }
            };
        }
        if ( this.recordContainer ) {
            representative.PATH_TARGET = true;
        }
        return representative;
    }
}

class ArrayClass {
    constructor( typesSet ) {
        this.type = "array";
        this.members = typesSet; 
        //this.name - added in getName()
        this.nullableInstanceCount = 0;
        this.valueTypes = new Map(); // (first) instance keyed (does not matter if first,last,or random, as instances are as identically matched as possible
        for ( let type of typesSet ) {
            if ( Array.isArray( type ) ) {
                throw new Error( "Unexpected: ArrayClass cannot be created with sub-arrays" );
            }
            this.valueTypes.set( type, { 
                instanceCount: 0
                //name: name
            } );
        }
        this.dominantType = new DominantType();
    }
    includes( type ) {
        return this.members.has( type );
    }
    // Returns a Map of the provided arrayList to ArrayClasses
    // An extra subArrayMap helps resolve the Type of any encountered array children of an array.
    static classify( arrayList, subArrayMap ) {
        let arrayClassMap = new Map();
        let arrayClassGroupsBySize = new Map(); // Convenience that helps identify existing ArrayClasses faster
        while ( arrayClassMap.size != arrayList.length ) { 
            // This terminates because no arrays contain circular references with child arrays (e.g., they are from JSON data)
            for ( let array of arrayList ) {
                if ( arrayClassMap.has( array ) ) {
                    break; // I've already processed this instance, then skip it.
                }
                // Reduce 'array' down to its unique types...
                let arrayClassTypes = new Set();
                let settled = true;
                let nullCount = 0;
                array.forEach( ( type ) => {
                    if ( Array.isArray( type ) ) {
                        // Is this an already (real/hypothetically) settled Type?
                        if ( subArrayMap.has( type ) ) {
                            type = subArrayMap.get( type );
                        }
                        else if ( arrayClassMap.has( type ) ) {
                            type = arrayClassMap.get( type );
                        }
                        else { // Haven't [yet] created a Type/Class for this instance...
                            type = "array";
                            settled = false;
                        }
                    }
                    else if ( type == undefined ) {
                        throw new Error( "Unexpected! undefined value in JSON array data!" );
                    }
                    if ( type == "null" ) {
                        nullCount++; // And omit it from the arrayClassTypes...
                    }                    
                    else {
                        arrayClassTypes.add( type );
                    }
                } );
                if ( nullCount == array.length ) { // An entire array full of nulls!
                    arrayClassTypes.add( "null" );
                }
                if ( settled ) {
                    if ( !arrayClassGroupsBySize.has( arrayClassMap.size ) ) {
                        arrayClassGroupsBySize.set( arrayClassTypes.size, [] );
                    }
                    let arrayClassGroup = arrayClassGroupsBySize.get( arrayClassTypes.size );
                    let found = false;
                    for ( let existingArrayClass of arrayClassGroup )  {
                        if ( existingArrayClass.matches( arrayClassTypes ) ) {
                            arrayClassMap.set( array, existingArrayClass );
                            existingArrayClass.addInstance( array );
                            found = true;
                            break;
                        }
                    }
                    if ( !found ) {
                        let newArrayClass = new ArrayClass( arrayClassTypes );
                        arrayClassMap.set( array, newArrayClass );
                        newArrayClass.addInstance( array );
                        arrayClassGroup.push( newArrayClass );
                    }
                }
                // If not settled, then this has to go-round again...
            }
        }
        return arrayClassMap;
    }
    // Already the relative sizes of the different type sets are factored out--they are the same when
    // this is called.
    matches( typeSet ) {
        for ( let type of typeSet ) {
            if ( !this.members.has( type ) ) {
                return false;
            }
        }
        return true;
    }
    fixup( objectMap, tempRecordMap ) {
        for ( let type of this.members ) {
            if ( tempRecordMap.has( type ) ) {
                let newType = objectMap.get( tempRecordMap.get( type ) );
                this.members.delete( type );
                this.members.add( newType );
                let state = this.valueTypes.get( type );
                this.valueTypes.delete( type );
                this.valueTypes.set( newType, state );
            }
        }
    }
    addInstance( array ) {
        let tracker = new InstanceTypeTracker();
        for ( let type of array ) {
            if ( type == "null" ) {
                this.nullableInstanceCount++;
                break;
            }
            this.valueTypes.get( type ).instanceCount++;
            tracker.add( type );
        }
        this.dominantType.update( tracker );
    }
    *getName( globalTypeMap, recursionBreaker, timer ) {
        if ( this.name ) {
            return this.name;
        }
        // Avoid potential infinate recursion...
        if ( recursionBreaker.has( this ) ) {
            let counter = recursionBreaker.get( "uniqueId" );
            let name = `arrayrecursive${ counter }`;
            recursionBreaker.set( "uniqueId", counter + 1 );
            return name;
        }
        recursionBreaker.set( this, true );
        // 1. Figure out the name of this arrayTemplate
        let typeNameList = [];
        for ( let [ type, statsOb ] of this.valueTypes ) {
            let typeName = yield* globalGetName( type, globalTypeMap, recursionBreaker, timer );
            statsOb.name = typeName;
            typeNameList.push( typeName );
            yield* pauseOpportunity( timer );
        }
        typeNameList.sort();
        this.name = `array<${ typeNameList.join( "|" ) }>`;
        globalTypeMap[ this.name ] = this;        
        return this.name;
    }
    containsRecordContainer( path ) {
        // If this has been previously visited, then return quickly.
        if ( this.pathChecked ) {
            return false; // Not on path or the previous visit would have found the path first
        }
        this.pathChecked = true;
        if ( this.recordContainer ) {
            path.unshift( new PathSeg( this.name, "null" ) );
            return true;
        }
        for ( let subtype of this.valueTypes.keys() ) {
            if ( findPath( subtype, path ) ) {
                path.unshift( new PathSeg( this.name, this.type ) );
                return true;
            }   
        }
        return false;
    }
    toJSON() {
        let representative = {
            type: this.type,
            subtypes: {},
            subtypesNullable: ( this.nullableInstanceCount > 0 ),
            stats: {
                nullableCount: this.nullableInstanceCount
            }
        };
        // Gather subtypes
        for ( let valueType of this.valueTypes.values() ) {
            representative.subtypes[ valueType.name ] = {
                instanceCount: valueType.instanceCount
            }
        }
        if ( this.recordContainer ) {
            representative.PATH_TARGET = true;
        }
        return representative;
    }
}

// Helper functions
function findPath( type, path ) {
    if ( typeof type == "string" ) {
        return false;
    }
    else { // It's a type object (object/array/class)
        return type.containsRecordContainer( path );
    }
}

// Returns one of: null|string|boolean|number|array|object|emptyarray|emptyobject
function getBasicType( test ) {
    // Note "symbol" won't happen because I don't attempt to extract symbols from the object (not valid JSON)
    if ( typeof test === "function" ) {
        throw Error("function instance encountered in data--invalid JSON");
    }
    if ( typeof test === "undefined" ) {
        throw Error("undefined value encountered in data--invalid JSON");
    }
    if ( test === null ) {
        return "null";
    }
    if ( typeof test !== "object" ) {
        if ( typeof test === "string" ) {
            // Try to find other data-types in disguise...
            if ( test === "true" || test === "false" ) {
                return "boolean";
            }
        }
        return typeof test;
    }
    if ( Array.isArray( test ) ) {
        if ( test.length == 0 ) {
            return "emptyarray";
        }
        return "array";
    }
    if ( Object.keys( test ).length == 0 ) {
        return "emptyobject";
    }
    return "object";
}

function isLeafType( typeString ) {
    switch ( typeString ) {
        case "null":
        case "string":
        case "number":
        case "boolean":
        case "emptyobject":
        case "emptyarray":
            return true;
        default:
            return false; // "array", "object"
    }
}

class PathSeg {
    constructor( name, typeStr, optionalNextProperty ) {
        this.type = name;
        switch ( typeStr ) {
            case "null":
            case "string":
            case "number":
            case "boolean":
                this.next = null;
                break;
            case "record":
            case "array":
                this.next = [];
                break;
            case "object":
            case "arraytype":
                if ( !optionalNextProperty ) {
                    throw new Error( "Next Property name required when type is 'object' or 'arraytype'" );
                }
                this.next = optionalNextProperty; // A "key" or "index"
                break;
        }
    }
}