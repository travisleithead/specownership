## Server-side Data

* `diff-schedule.json` - the next [future] date/time of when to perform each data-source's diff. This
    enables the server to be temporarily shut-down and restarted without losing its schedule (or to
    know if it need to do a catch-up diff of various datasources after being shutdown for a long time).
* `datasource.index.json` - master index of all datasources making up the linkable superset. A 
   datasource must be configured in the index in order to be usable as a linkable type. The index has
   the file name location of all configured datasources that the server is aware of.
* `<datasource-file>.config.json` - a file containing:
  * the datasource URL
  * the update frequency (a relative duration)
  * the latest schema for the datasource (if schema-differences change over time)
  * the path to navigate the datasource to find its record container (this is also the unique key for
     the datasource records)
  * which record values can be ignored by the diff-engine (because their diff doesn't matter)
* `<datasource-file>.data.json` - a local copy of the latest remote datasource, with diff-history
   annotations added to each record.
* `<datasource-file>.data.backup.json` - a copy of the existing data structure before it is modified, 
   in case of processing bugs that cause corruption, this data can be restored.

## Service entry points

### Setup a new linkable data source

```
 Entry Point: /new-datasource
```

Initially, the linking server starts out with no datasources. Use this service to add a new linkable
data source to the system. Linkable data sources must be JSON. Adds an entry to `datasource.index.json`
a new `<datasource-file>.config.json` and `<datasource-file>.data.json` file.

#### Server-side Processes

* `schema-generator` - takes a JSON data source and derives a generic schema for it. Identifies the
   records contained in the data source, and the path to the records. Returns the path and schema.
* 

## Background services

* `diff-engine` - 
