testSchemaGeneratorAsync = require("./test-schema-generator");

// Starting point for the app
(async () => {
    console.log( "Booting up..." );
    console.log( "* Running test validation for schema generator" );
    
    let testsPassedCount = await testSchemaGeneratorAsync();
    console.log(`  Tests passed: ${ testsPassedCount }` );

    console.log( "* Running test validation for diff engine" );
    await require("./test-diff-engine")();

    console.log("Starting Server...")
    require("./server");
    console.log("SpecOwnership NodeJS Server Started at 127.0.0.1:8080");
})();