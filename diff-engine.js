// Diff engine

module.exports = exports = class DiffEngine {
    constructor(remoteDataSource, localDataSource) {
        if (typeof remoteDataSource == "undefined" || typeof localDataSource == "undefined")
            throw Error("Constructor requires a remoteDataSource and localDataSource parameters");
        this[Symbol.for("diff.engine.ready")] = {
            local: localDataSource,
            remote: remoteDataSource
        };
    }
    // This method modifies the local data source, annotating it with the differences found while processing.
    asyncDiff() {
        // Don't start if the instance isn't ready...
        if (!this[Symbol.for("diff.engine.ready")])
            throw Error("DiffEngine not constructed--call new DiffEngine() first");
        // Do the next part asyncronously
        return new Promise((resolve, reject) => {
            let state = this[Symbol.for("diff.engine.ready")]; // Yea! for Lexically-scoped 'this' in Arrow functions!
            setTimeout(function resumeFunc() {
                if (!state.resume) {
                    // If this is the first time through the timeout...
                    state.resume = interruptableDiff(state);
                }
                let result = state.resume.next();
                if (!result.done) {
                    setTimeout(resumeFunc, 0); // Loop around again in the next turn of the event loop
                }
                else
                    resolve(result.value);
            }, 0);
        })
    }
}

let DIFF_CONSECUTIVE_PROCESSING_LIMIT = 2;

// Generator that supports periodic yeilding to free-up the main execution thread
function* interruptableDiff(state) {
    let local = state.local;
    let remote = state.remote;
    // Iterate keys or iterate an array...
    if (Array.isArray(local)) {
        for (let i = 0, len = local.length; i < len; i++) {
            let val = local[i];

            if ((i % DIFF_CONSECUTIVE_PROCESSING_LIMIT) == 0) {
                yield;
            }
        }
    }
    else if ((typeof local == "object") && (local !== null)) {
        // Iterate object keys...
        let keys = Object.keys(local);
        for (let i = 0, len = keys.length; i < len; i++) {
            let val = local[keys[i]];

            if ((i % DIFF_CONSECUTIVE_PROCESSING_LIMIT) == 0) {
                yield;
            }
        }
    }
    else
        throw Error("asyncDiff can't iterate the data source (may need a schema hint)");
    
    return 10;
}

// Checks to see if the things are of the same type
function compareType(localThing, remoteThing) {
    if (typeof localThing == typeof remoteThing)
        return true;

}

