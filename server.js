var http = require("http"); // https://nodejs.org/api/http.html

var server = http.createServer( (request, response) => {
    var url = request.url;
    // request from client.
    response.writeHead(200, {
        "Content-Type": "text/plain"
    })
    response.write("hello world!");
    response.end(`\nRequest URL: ${url}`);
});

server.listen(8080, '127.0.0.1');