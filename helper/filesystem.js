// Promise-wraps the built-in filesystem Async API used by this application
const fs = require( "fs" );

module.exports = exports = {
    readFile( path, options ) {
        return new Promise( ( resolve, reject ) => {
            let callback = ( err, data ) => {
                if ( err ) {
                    reject( err );
                }
                else {
                    resolve( data );
                }
            }
            if ( typeof options != "undefined" ) {
                fs.readFile( path, options, callback );
            }
            else {
                fs.readFile( path, callback );
            }
        } );
    },
    writeFile( path, data, options ) {
        return new Promise( ( resolve, reject) => {
            let callback = ( err, data ) => {
                if ( err ) {
                    reject( err );
                }
                else {
                    resolve( data );
                }
            }
            if ( typeof options != "undefined" ) {
                fs.writeFile( path, data, options, callback );
            }
            else {
                fs.writeFile( path, data, callback );
            }
        } );
    }
}