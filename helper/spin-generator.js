// Helper class to process a generator function
module.exports = exports = async function( iterator ) {
    return new Promise( ( resolve ) => {
        setTimeout( function spinIteratorGeneric() {
            let result = iterator.next();
            if ( !result.done ) {
                setTimeout( spinIteratorGeneric, 0);
            }
            else {
                resolve(result.value);
            }
        }, 0);
    } );
}