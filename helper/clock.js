const { performance } = require( "perf_hooks" );

// Helper that maintains a timer. One use case is to ensure big workloads don't run for too long
// by enabling time-slicing in generator functions based on this clock.
module.exports = exports = class {
    // The relative stop time in milliseconds
    constructor( relativeStopTimeMS ) {
        this.incrementBy = relativeStopTimeMS;
        this.reset();
    }
    isExpired() {
        return ( performance.now() > this.stopAfter );
    }
    reset() {
        this.stopAfter = performance.now() + this.incrementBy;
    }
}